package sse643.project1.hatfield;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.input.InputManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;

/**
 * Meshes, shapes, solid colors to form border around Georgia elevation map
 * and hide ocean underneath
 * 
 * @author Hatfield, Kevin
 * 
 * @see AbstractAppState
 * @see <a href="http://jmonkeyengine.org">JMonkey Engine</a>
 */

public class TerraBorders extends AbstractAppState implements Config
{
	private SimpleApplication _app;
	private Node _rootNode;
	private AssetManager _assetManager;
	private AppStateManager _appStateManager;
	private InputManager _inputManager;
	private ViewPort _viewPort;
	private BulletAppState _bulletAppState;
	
	@Override
	public void initialize(AppStateManager stateManager, Application appIn)
	{
		super.initialize(stateManager, appIn);
		
		this._app = (SimpleApplication) appIn;
		
		this._rootNode = this._app.getRootNode();
		
		this._assetManager = this._app.getAssetManager();
		
		this._appStateManager = this._app.getStateManager();
		
		this._inputManager = this._app.getInputManager();
		
		this._viewPort = this._app.getViewPort();
		
		this._bulletAppState = 
				this._appStateManager.getState(BulletAppState.class);
	
		createTerraBorders();
		
	}//init method
	
		
	private void createTerraBorders()
	{
		/* ensure files available */

        final Material stateBorderMaterial = new Material(
        		getAssetManager(), 
        		Config.filenames.UNSHADED_MATERIAL_DEF.txt());

        final Material stateBorderConstructionMaterial = new Material(
        		getAssetManager(), 
        		Config.filenames.UNSHADED_MATERIAL_DEF.txt());

        /* bordering states: thick meshes extending beyond horizon */
        
        /* bordering states: custom color */
        
		final ColorRGBA colorStateBorder = new ColorRGBA();
        colorStateBorder.fromIntRGBA(Config.colors.BORDERLANDS.num());

        /* tweak */
        
        stateBorderMaterial.getAdditionalRenderState().
        		setBlendMode(BlendMode.Off);

        stateBorderMaterial.setColor("Color", colorStateBorder);

        
        // debug aid: visible construction block mesh
        
        stateBorderConstructionMaterial.getAdditionalRenderState().
        		setBlendMode(BlendMode.Off);
        
        stateBorderConstructionMaterial.setColor("Color", ColorRGBA.Red);          
        
        
        /* create and position meshes */
        
        final Box stateBorderSurface = new Box(5000f, 5f, 5000f);
        
        /* south */
        
        final Geometry stateBorderSouthGeometry = new Geometry("stateBorderS", 
        		stateBorderSurface);
        
        stateBorderSouthGeometry.setMaterial(stateBorderMaterial);
        
        stateBorderSouthGeometry.setLocalTranslation(-4670, 0f, 5460);
        

        /* north */
        
        final Geometry stateBorderNorthGeometry = new Geometry("stateBorderN", 
        		stateBorderSurface);
        
        stateBorderNorthGeometry.setMaterial(stateBorderMaterial);
        
        stateBorderNorthGeometry.setLocalTranslation(-4557, 0f, -4855);

        
        /* west */
        
        final Geometry stateBorderWestGeometry = new Geometry("stateBorderW", 
        		stateBorderSurface);
        
        stateBorderWestGeometry.setMaterial(stateBorderMaterial);
        
        stateBorderWestGeometry.setLocalTranslation(-4669, 0, -20);


        /* non-root node used to build with */
  	  
        final Node bordersNode = new Node("Scene");

        
        /* make meshes solid: prevent observer falling through borders */

        /* north */ 
        
        final CollisionShape stateBorderNorthShape = CollisionShapeFactory.
        		createMeshShape(stateBorderNorthGeometry);
                
        final RigidBodyControl stateBorderNorthLandscape = 
        		new RigidBodyControl(stateBorderNorthShape, 0);	//zero mass
                
        bordersNode.addControl(stateBorderNorthLandscape);

        /* south */ 
        
        final CollisionShape stateBorderSouthShape = CollisionShapeFactory.
        		createMeshShape(stateBorderSouthGeometry);
        
        final RigidBodyControl stateBorderSouthLandscape = 
        		new RigidBodyControl(stateBorderSouthShape, 0);
        
        bordersNode.addControl(stateBorderSouthLandscape);

        /* west */
        
        final CollisionShape stateBorderWestShape = CollisionShapeFactory.
        		createMeshShape(stateBorderWestGeometry);
        
        final RigidBodyControl stateBorderWestLandscape = 
        		new RigidBodyControl(stateBorderWestShape, 0);
        
        bordersNode.addControl(stateBorderWestLandscape);


        /* place in temporary node */
        
        bordersNode.attachChild(stateBorderSouthGeometry);

        bordersNode.attachChild(stateBorderNorthGeometry);
        
        bordersNode.attachChild(stateBorderWestGeometry);
        

        /* 
         * meshes underneath beach: 
         * 		prevent future glitches, swimmer getting underneath state lands
         */

        // box 1
        
        final Box stateBorderBeach1Surface = new Box(17f, 4f, 92f);
        
        final Geometry stateBorderBeach1Geometry = 
        		new Geometry("stateBorder", stateBorderBeach1Surface);
        
        stateBorderBeach1Geometry.setMaterial(stateBorderMaterial);
        
        stateBorderBeach1Geometry.setLocalTranslation(350, 0, 244);
        
        bordersNode.attachChild(stateBorderBeach1Geometry);
        
        // box 2
        
        final Box stateBorderBeach2Surface = new Box(17f, 4f, 62f);
        
        final Geometry stateBorderBeach2Geometry = 
        		new Geometry("stateBorder", stateBorderBeach2Surface);
        
        stateBorderBeach2Geometry.setMaterial(stateBorderMaterial);
        
        /* 
         * vector addition, place mesh against previous: adjacent stacking 
         * underneath beach 
         */
        
        stateBorderBeach2Geometry.setLocalTranslation( 
        		stateBorderBeach1Geometry.getLocalTranslation().
        		add(new Vector3f(34,0,-31)) );
        
        bordersNode.attachChild(stateBorderBeach2Geometry);

        // box 3

        final Box stateBorderBeach3Surface = new Box(12f, 4f, 26f);
        
        final Geometry stateBorderBeach3Geometry = 
        		new Geometry("stateBorder", stateBorderBeach3Surface);
        
        stateBorderBeach3Geometry.setMaterial(stateBorderMaterial);
        
        stateBorderBeach3Geometry.setLocalTranslation( 
        		stateBorderBeach2Geometry.getLocalTranslation().
        		add(new Vector3f(30,0,-35)) );
        
        bordersNode.attachChild(stateBorderBeach3Geometry);
		
        
        /* place into the root node */
        
        getRootNode().attachChild(bordersNode);
        
        /* wait to modify until landscapes have been placed into root node, 
         * although seems to not affect either way 
         */
        
        getBulletAppState().getPhysicsSpace().add(stateBorderNorthLandscape);
        getBulletAppState().getPhysicsSpace().add(stateBorderWestLandscape);
        getBulletAppState().getPhysicsSpace().add(stateBorderSouthLandscape);
        
	}//init method, local
	
	
	/* encapsulation methods */
	
	
	@SuppressWarnings("unused")
	private SimpleApplication getApp(){ return _app; }
	
	private Node getRootNode(){ return _rootNode; }
	
	private AssetManager getAssetManager(){ return _assetManager; }
	
	@SuppressWarnings("unused")
	private AppStateManager getAppStateManager(){ return _appStateManager; }
	
	@SuppressWarnings("unused")
	private InputManager getInputManager(){ return _inputManager; }
	
	@SuppressWarnings("unused")
	private ViewPort getViewPort(){ return _viewPort; }
	
	private BulletAppState getBulletAppState(){ return _bulletAppState; }

	
}//class
