package sse643.project1.hatfield;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.input.InputManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.BloomFilter;
import com.jme3.post.filters.DepthOfFieldFilter;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture2D;
import com.jme3.water.WaterFilter;

/**
 * Atlantic ocean and related features
 * 
 * @author Hatfield, Kevin
 * 
 * @see AbstractAppState
 * @see <a href="http://jmonkeyengine.org">JMonkey Engine</a>
 */

public class Ocean extends AbstractAppState implements Config
{
	private WaterFilter _water;
	
    private float _waveTime;
    private float _waterHeight;
    private boolean _underwater;
	
	private SimpleApplication _app;
	private Node _rootNode;
	private AssetManager _assetManager;
	private AppStateManager _appStateManager;
	private InputManager _inputManager;
	private ViewPort _viewPort;
	private BulletAppState _bulletAppState;
	
	@Override
	public void initialize(AppStateManager stateManager, Application appIn)
	{
		super.initialize(stateManager, appIn);
		
		this._app = (SimpleApplication) appIn;
		
		this._rootNode = this._app.getRootNode();
		
		this._assetManager = this._app.getAssetManager();
		
		this._appStateManager = this._app.getStateManager();
		
		this._inputManager = this._app.getInputManager();
		
		this._viewPort = this._app.getViewPort();
		
		this._bulletAppState = 
				this._appStateManager.getState(BulletAppState.class);
	
		createOcean();
		
	}//init method
	
	
	private void createOcean()
	{		
		_waveTime = 0.0f;
		_waterHeight = 0.0f;
		_underwater = false;
		
		_water = new WaterFilter(getRootNode(), Config.vectors3f.
				SUN_DIRECTION.val());

		//_water.setWaterTransparency(0.1f);
		//_water.setShoreHardness(0.1f);
		//_water.setUseHQShoreline(true);		
		//_water.setDeepWaterColor(ColorRGBA.Blue);

		final FilterPostProcessor afterFilter = 
				new FilterPostProcessor(getAssetManager());

		afterFilter.addFilter(_water);
		
		//wave crest against shore
		
		final BloomFilter waveBloom = new BloomFilter();

		waveBloom.setExposurePower(18);
		waveBloom.setBloomIntensity(0.2f);
		afterFilter.addFilter(waveBloom);

		//@TODO light scatter effect caused dimness for certain camera angles
		
		final DepthOfFieldFilter oceanGazeFilter = new DepthOfFieldFilter();
		oceanGazeFilter.setFocusDistance(1);
		oceanGazeFilter.setFocusRange(400);
		afterFilter.addFilter(oceanGazeFilter);

		_water.setWaveScale(0.009f);
		_water.setMaxAmplitude(1.8f);
		_water.setFoamExistence(Config.vectors3f.SEA_FOAM_EXIST.val());
		_water.setFoamTexture((Texture2D) getAssetManager().loadTexture(
				Config.filenames.SEA_FOAM.txt()));
		_water.setNormalScale(0.8f);

		_water.setRefractionStrength(0.2f);
		//_water.setFoamHardness(0.3f);

		_water.setWaterHeight(Config.misc.INITIAL_WATER_HEIGHT.flt());

		_water.setWindDirection(Config.vectors2f.WIND_DIRECTION.val());

		/* apply */
		
		getViewPort().addProcessor(afterFilter);

		/* other water features */

		fish();
		
	}//init method, local

	
	@Override
    public void update(float tpf) 
    {
		//tides or waves against shore
		
        getApp().simpleUpdate(tpf);

        _waveTime += tpf;
        _waterHeight = (float) 
        		Math.cos(((_waveTime * 0.6f) % FastMath.TWO_PI)) * 1.5f;
        
        _water.setWaterHeight(Config.misc.
        		INITIAL_WATER_HEIGHT.flt() + _waterHeight);
        
        //@TODO should use some xor? or other conditional
        
        if(_water.isUnderWater() && !_underwater) 
        {

            _underwater = true;
        
        }else if(!_water.isUnderWater() && _underwater) 
        {
            _underwater = false;

        }//if
				
    }//method
	
	
	private void fish()
	{	
		/* ensure files available */
		
        final Spatial shark = getAssetManager().
        		loadModel(Config.filenames.SHARK_MODEL.txt());

        final Material sharkSkin = new Material(getAssetManager(),
        		Config.filenames.UNSHADED_MATERIAL_DEF.txt());
        
        /* tweak */
        
        shark.setLocalScale(2);
        
        shark.setLocalTranslation(Config.vectors3f.SHARK_POSITION.val());
        
        sharkSkin.setColor("Color", ColorRGBA.Gray);
        
        sharkSkin.getAdditionalRenderState().setBlendMode(BlendMode.Off);
        
        shark.setMaterial(sharkSkin);
        
        /* apply */
        
        getRootNode().attachChild(shark);
		
	}//method

		
	/* encapsulation methods */
	
	
	private SimpleApplication getApp(){ return _app; }
	
	private Node getRootNode(){ return _rootNode; }
	
	private AssetManager getAssetManager(){ return _assetManager; }
	
	@SuppressWarnings("unused")
	private AppStateManager getAppStateManager(){ return _appStateManager; }
	
	@SuppressWarnings("unused")
	private InputManager getInputManager(){ return _inputManager; }
	
	private ViewPort getViewPort(){ return _viewPort; }
	
	@SuppressWarnings("unused")
	private BulletAppState getBulletAppState(){ return _bulletAppState; }

	
}//class
