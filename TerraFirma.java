package sse643.project1.hatfield;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.HeightfieldCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.input.InputManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.terrain.geomipmap.TerrainLodControl;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.terrain.geomipmap.lodcalc.DistanceLodCalculator;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import com.jme3.terrain.heightmap.ImageBasedHeightMap;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture.WrapMode;

/**
 * Use AlphaMap, HeightMap to create terrain
 * 
 * @author Hatfield, Kevin
 * 
 * @see AbstractAppState
 * @see <a href="http://jmonkeyengine.org">JMonkey Engine</a>
 */

public class TerraFirma extends AbstractAppState implements Config
{
	private SimpleApplication _app;
	private Node _rootNode;
	private AssetManager _assetManager;
	private AppStateManager _appStateManager;
	private InputManager _inputManager;
	private ViewPort _viewPort;
	private BulletAppState _bulletAppState;
	
	@Override
	public void initialize(AppStateManager stateManager, Application appIn)
	{
		super.initialize(stateManager, appIn);
		
		this._app = (SimpleApplication) appIn;
		
		this._rootNode = this._app.getRootNode();
		
		this._assetManager = this._app.getAssetManager();
		
		this._appStateManager = this._app.getStateManager();
		
		this._inputManager = this._app.getInputManager();
		
		this._viewPort = this._app.getViewPort();
		
		this._bulletAppState = 
				this._appStateManager.getState(BulletAppState.class);
	
		createTerraFirma();
		
	}//init method
	
	
	private void createTerraFirma()
	{
		/* ensure files available */
		
		final Material terraMap = new Material( getAssetManager(), 
				Config.filenames.TERRA_MATERIAL_DEF.txt() );

		final Texture heightMapTexture = getAssetManager().
				  loadTexture(Config.filenames.HEIGHT_MAP.txt());

		terraMap.setTexture(
	    		  "AlphaMap", getAssetManager().
	    		  loadTexture(Config.filenames.ALPHA_MAP.txt()));

		final Texture low = getAssetManager().
				loadTexture(Config.filenames.TERRA_LOW.txt());
		
		final Texture medium = getAssetManager().
				loadTexture(Config.filenames.TERRA_MEDIUM.txt());

		final Texture high = getAssetManager().
				loadTexture(Config.filenames.TERRA_HIGH.txt());
		
		
		/* remove black remnants found on alphaMap image borders */

		final RenderState terraMapRenderState = 
				terraMap.getAdditionalRenderState();
		
		terraMapRenderState.setAlphaTest(true);
		terraMapRenderState.setAlphaFallOff(.1f);
		
		terraMapRenderState.setBlendMode(BlendMode.Alpha);

		/* false : causes tiles to lack any detail except color */
		
		terraMap.setBoolean("useTriPlanarMapping", true);
		
		/* alpha map: set splat textures to alpha map colors */

		terraMap.setFloat("DiffuseMap_2_scale", 1f);	//red
	    terraMap.setTexture("DiffuseMap_2", low);
		
	    terraMap.setFloat("DiffuseMap_0_scale", 1f);	//blue
	    terraMap.setTexture("DiffuseMap", high);

	    terraMap.setFloat("DiffuseMap_1_scale", 1f);	//green
	    terraMap.setTexture("DiffuseMap_1", medium);

		low.setWrap(WrapMode.Repeat);
		medium.setWrap(WrapMode.Repeat);
		high.setWrap(WrapMode.Repeat);

		
	    /* height map: load, tweak */
	    
	    final AbstractHeightMap heightMap = 
	    		new ImageBasedHeightMap(heightMapTexture.getImage());
	    
	    heightMap.setHeightScale(22);
	    heightMap.load();

	    final TerrainQuad terraQuad = new TerrainQuad(
	    		  "terrain", 1025, 1025, heightMap.getHeightMap());
    
	    final TerrainLodControl terraLodCtrl = new TerrainLodControl(
	    		terraQuad, getApp().getCamera());

	    /* 
	     * patch size dependent on the overall 1025 terrain size,
	     * if less than 1/4 terrain then only portion of terrain shown
	     */

	    terraLodCtrl.setLodCalculator(new DistanceLodCalculator(512, 2.7f));
	      
	    terraQuad.addControl(terraLodCtrl);
 
	    
	    /* alpha map: apply splat textures to terrain */
	      
	    terraQuad.setMaterial(terraMap);
	    
	    /* @TODO terrain absorbs shadows sometimes dropped, alpha fall-off? */
	    
	    //terraQuad.setShadowMode(ShadowMode.Receive);	

	    
	    /* attach terrain to root node */
	    
	    
	    getRootNode().attachChild(terraQuad);
	    
	    
	    /* make solid: prevent observer falling through */
	    
        final HeightfieldCollisionShape terraShape = 
        		new HeightfieldCollisionShape(		
        		terraQuad.getHeightMap(), terraQuad.getLocalScale());
        
        RigidBodyControl terraLandscape = 
        		new RigidBodyControl(terraShape, 0);	//zero mass	
	    
        getBulletAppState().getPhysicsSpace().add(terraLandscape);
        
	}//init method, local
	
	
	/* encapsulation methods */
	
	
	private SimpleApplication getApp(){ return _app; }
	
	private Node getRootNode(){ return _rootNode; }
	
	private AssetManager getAssetManager(){ return _assetManager; }
	
	@SuppressWarnings("unused")
	private AppStateManager getAppStateManager(){ return _appStateManager; }
	
	@SuppressWarnings("unused")
	private InputManager getInputManager(){ return _inputManager; }
	
	@SuppressWarnings("unused")
	private ViewPort getViewPort(){ return _viewPort; }
	
	private BulletAppState getBulletAppState(){ return _bulletAppState; }

	
}//class
