package sse643.project1.hatfield;

import com.jme3.app.SimpleApplication;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;

/**
 * Configuration constants
 * 
 * @author Hatfield, Kevin
 * 
 * @see SimpleApplication
 * @see <a href="http://jmonkeyengine.org">JMonkey Engine</a>
 */

public interface Config 
{
	static enum vectors2f
	{
		WIND_DIRECTION (new Vector2f(0.8f,0f));

		
		private Vector2f _vecVal;

		
		/* constructors */
		
		vectors2f(Vector2f valIn)
		{
			_vecVal = valIn;
			
		}//constructor
		
		
		/* encapsulation methods */
		
		
		Vector2f val()
		{
			return _vecVal;
			
		}//method
		
	}//enum
	
	
	static enum vectors3f
	{
		SEA_FOAM_EXIST (new Vector3f(1f, 4, 0.5f)),
		START_POSITION (new Vector3f(-100, 790, 248)),
		START_LOOK_DIR (new Vector3f(0, -1.5f, -1)),
		SHARK_POSITION (new Vector3f(700, -8, 300)),
		SUN_DIRECTION (new Vector3f(0f, -0.7f, -0.3f));
		
		private Vector3f _vecVal;

		
		/* constructors */
		
		vectors3f(Vector3f valIn)
		{
			_vecVal = valIn;
			
		}//constructor
		
		
		/* encapsulation methods */
		
		
		Vector3f val()
		{
			return _vecVal;
			
		}//method
		
	}//enum

	
	static enum misc
	{
		FRUSTUM_FAR (4000),
		INITIAL_WATER_HEIGHT (0.9f);
		
		private Integer _intVal;
		private Float _floatVal;
		
		/* constructors */
		
		misc(Integer valIn)
		{
			_intVal = valIn;
			
		}//constructor
		
		misc(Float valIn)
		{
			_floatVal = valIn;
			
		}//constructor
		
		/* encapsulation methods */
		
		
		Integer num()
		{
			return _intVal;
			
		}//method
		
		Float flt()
		{
			return _floatVal;
			
		}//method
		
	}//enum
	
	
	static enum colors
	{
		BORDERLANDS (7172454);
		
		private Integer _intVal;

		
		/* constructors */
		
		colors(Integer valIn)
		{
			_intVal = valIn;
			
		}//constructor
		
		
		/* encapsulation methods */
		
		
		Integer num()
		{
			return _intVal;
			
		}//method
		
	}//enum
	
	
	static enum filenames
	{ 
		TERRA_MATERIAL_DEF ("Common/MatDefs/Terrain/TerrainLighting.j3md"),
		UNSHADED_MATERIAL_DEF ("Common/MatDefs/Misc/Unshaded.j3md"),
		HEIGHT_MAP ("Custom/heightmapGeorgiaVer015.png"),
		ALPHA_MAP ("Custom/alphamapGeorgiaVer016.png"),
		TERRA_LOW ("Custom/ivoryVer001.png"),
		TERRA_MEDIUM ("Custom/paleVer002.png"),
		TERRA_HIGH ("Custom/brownVer001.png"),
		SEA_FOAM ("Common/MatDefs/Water/Textures/foam2.jpg"),
		SKY_CLOUDS ("Custom/blueskyclouds.png"),
		SKY_SUN ("Custom/blueskysun.png"),
		SKY_BIRDS ("Custom/blueskybirds.png"),
		SKY_BLUE ("Custom/blueskynosun.png"),
		SHARK_MODEL ("Custom/Shark/Shark.obj"),;
		
		private String _strVal;

		
		/* constructors */
		
		
		filenames(String strValueIn)
		{
			_strVal = strValueIn;
			
		}//constructor
		
		
		/* encapsulation methods */
		
		
		String txt()
		{ 
			return _strVal; 
		
		}//method
				
	 }//enum

}//interface
