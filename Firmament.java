package sse643.project1.hatfield;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.input.InputManager;
import com.jme3.light.DirectionalLight;
import com.jme3.math.Vector3f;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;

/**
 * Use separate images to create seam-free sky with sun, birds, clouds
 * 
 * @author Hatfield, Kevin
 * 
 * @see AbstractAppState
 * @see <a href="http://jmonkeyengine.org">JMonkey Engine</a>
 */

public class Firmament extends AbstractAppState implements Config
{
	private Spatial _sky;
	
	private SimpleApplication _app;
	private Node _rootNode;
	private AssetManager _assetManager;
	private AppStateManager _appStateManager;
	private InputManager _inputManager;
	private ViewPort _viewPort;
	private BulletAppState _bulletAppState;
	
	@Override
	public void initialize(AppStateManager stateManager, Application appIn)
	{
		super.initialize(stateManager, appIn);
		
		this._app = (SimpleApplication) appIn;
		
		this._rootNode = this._app.getRootNode();
		
		this._assetManager = this._app.getAssetManager();
		
		this._appStateManager = this._app.getStateManager();
		
		this._inputManager = this._app.getInputManager();
		
		this._viewPort = this._app.getViewPort();
		
		this._bulletAppState = 
				this._appStateManager.getState(BulletAppState.class);
	
		createFirmament();
		
	}//init method
	
	
	private void createFirmament()
	{
		/* sky: ensure files available */
		
		final Texture cloudsSky = getAssetManager().
				loadTexture(Config.filenames.SKY_CLOUDS.txt());

		final Texture sunSky = getAssetManager().
				loadTexture(Config.filenames.SKY_SUN.txt());
  	  
		final Texture clearSky = getAssetManager().
				loadTexture(Config.filenames.SKY_BLUE.txt());
  	  
		final Texture birdsSky = getAssetManager().
				loadTexture(Config.filenames.SKY_BIRDS.txt());

        final Spatial sky = SkyFactory.createSky(getAssetManager(), 
        		cloudsSky, birdsSky, clearSky, cloudsSky, sunSky, clearSky);
  	  
        /* sky: tweak */
      
        _sky = sky;
        
        sky.setQueueBucket(Bucket.Sky);
        sky.setLocalScale(1);
        sky.setCullHint(CullHint.Never);

        /* sky: apply to scene */
        
        getRootNode().attachChild(sky);
	    
        /* sun, lighting */
        
        final Vector3f sunDirection = new Vector3f(0f, -0.7f, -0.3f);
        
        final DirectionalLight sunLight = new DirectionalLight();
        sunLight.setDirection(sunDirection.normalize());
        
        getRootNode().addLight(sunLight);
        
	}//init method, local
	
	
	/* encapsulation methods */

	protected Spatial getSky(){ return _sky; }
	
	
	@SuppressWarnings("unused")
	private SimpleApplication getApp(){ return _app; }
	
	private Node getRootNode(){ return _rootNode; }
	
	private AssetManager getAssetManager(){ return _assetManager; }
	
	@SuppressWarnings("unused")
	private AppStateManager getAppStateManager(){ return _appStateManager; }
	
	@SuppressWarnings("unused")
	private InputManager getInputManager(){ return _inputManager; }
	
	@SuppressWarnings("unused")
	private ViewPort getViewPort(){ return _viewPort; }
	
	@SuppressWarnings("unused")
	private BulletAppState getBulletAppState(){ return _bulletAppState; }
	
}//class
