package sse643.project1.hatfield;

import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.system.AppSettings;

/**
 * Setup JMonkey engine environment<br><br>
 * 
 * Launch the GA Elevation Map application<br><br>
 * 
 * Use AppStates to divide application into classes
 * 
 * @author Hatfield, Kevin
 * 
 * @see SimpleApplication
 * @see <a href="http://jmonkeyengine.org">JMonkey Engine</a>
 */

public class GaElevMap extends SimpleApplication implements ActionListener
{
	private CharacterControl _observer;
	
	private boolean _left, _right, _up, _down;
	
	private Vector3f _navDir;
	private Vector3f _cameraDir;
	private Vector3f _cameraLeft;	
	
	protected BulletAppState _physicsState;
	
    @Override
    public void initialize() {

  	  super.initialize();
    
    }//init method
    
    
	public static void main(String arg[])
	{
		GaElevMap selfGaElevMap = new GaElevMap();
		
		selfGaElevMap.setDisplayStatView(false);
		selfGaElevMap.setShowSettings(false);
		
		AppSettings simulatorSettings = new AppSettings(true);
		simulatorSettings.setResolution(1280,720);
		simulatorSettings.setAudioRenderer(null);
		selfGaElevMap.setSettings(simulatorSettings);
		
		selfGaElevMap.start();

	}//method
	
	
	@Override
	public void simpleInitApp()
	{	
		_physicsState = new BulletAppState();
		stateManager.attach(_physicsState);
		
		stateManager.attach(new TerraFirma());
		stateManager.attach(new TerraBorders());
		stateManager.attach(new Ocean());
		stateManager.attach(new Firmament());

		createObserver();
		
	}//method


    /** record for fly camera movement follows observer */
	
    @Override
    public void onAction(String binding, boolean isPressed, float tpf) 
    {
      if(binding.equals("Left")) _left = isPressed;
      
      else if(binding.equals("Right")) _right = isPressed;
      
      else if(binding.equals("Up")) _up = isPressed;
      
      else if(binding.equals("Down")) _down = isPressed;
      
      else if(binding.equals("Jump") && isPressed) getObserver().jump();
      
    }//method	
	    
	
	private void createObserver()
	{		
		_navDir = new Vector3f();
		_cameraDir = new Vector3f();
		_cameraLeft = new Vector3f();
	
		/* camera position */

		final Camera cam = this.getCamera();

		cam.setLocation(Config.vectors3f.START_POSITION.val());
		
		cam.lookAtDirection(Config.vectors3f.
				START_LOOK_DIR.val().normalizeLocal(), 
				Vector3f.UNIT_Y);

		cam.setFrustumFar(Config.misc.FRUSTUM_FAR.num());
		
		/* observer: prevent falling into collision enabled surfaces */

		final CapsuleCollisionShape capsuleShape = 
				new CapsuleCollisionShape(1.5f, 6f, 1);
		
		final CharacterControl observer = 
				new CharacterControl(capsuleShape, 0.05f);
		
		observer.setJumpSpeed(2f);
		observer.setFallSpeed(2f);
		observer.setGravity(0f);
		observer.setPhysicsLocation(Config.vectors3f.START_POSITION.val());
     
		_observer = observer;
		
		_physicsState.getPhysicsSpace().add(observer);

		/* restrict, allow movements */
		
		setUpKeys();
				
	}//init method, local
	
	
	/** prevent fall through: disable some defaults for fly camera */
	
    private void setUpKeys() 
    {
    	final InputManager inptMgr = this.getInputManager();

    	inptMgr.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
    	inptMgr.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
    	inptMgr.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
    	inptMgr.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
    	inptMgr.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
    	
    	inptMgr.addListener(this, "Left");
    	inptMgr.addListener(this, "Right");
    	inptMgr.addListener(this, "Up");
    	inptMgr.addListener(this, "Down");
    	inptMgr.addListener(this, "Jump");
    
    }//method
    
    
    @Override
    public void simpleUpdate(float tpf) 
    {
    	super.simpleUpdate(tpf);
    	
    	final Camera cam = this.getCamera();
    	
        _cameraDir.set(cam.getDirection().mult(1.6f));
        _cameraLeft.set(cam.getLeft().multLocal(1.4f));

        _navDir.set(0, 0, 0);
        
        if(_left) _navDir.addLocal(_cameraLeft);
        
        else if(_right) _navDir.addLocal(_cameraLeft.negate());
        
        else if(_up) _navDir.addLocal(_cameraDir);
        
        else if(_down) _navDir.addLocal(_cameraDir.negate());
        
        _observer.setWalkDirection(_navDir);
        
        cam.setLocation(_observer.getPhysicsLocation());
    	
    }//method

    
	/* encapsulation methods */
	
    
	private CharacterControl getObserver(){ return _observer; }

}//class
